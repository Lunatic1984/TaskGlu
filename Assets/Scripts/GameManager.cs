﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Text score1Text;
    [SerializeField]
    private Text score2Text;
    [Header("Weapon panel")]
    [SerializeField]
    private GameObject weaponPanel;
    [SerializeField]
    private GameObject[] weaponButtons;
    [Header("AI choice panel")]
    [SerializeField]
    private GameObject aiWeaponPanel;
    [SerializeField]
    private Image playerWeaponImage;
    [SerializeField]
    private Image aiWeaponImage;
    [Header("Result panel")]
    [SerializeField]
    private GameObject resultPanel;
    [SerializeField]
    private Image playerWeaponResultImage;
    [SerializeField]
    private Image aiWeaponResultImage;
    [SerializeField]
    private RectTransform signTransform;
    [SerializeField]
    private Text resultText;
    [Header("Score change panel")]
    [SerializeField]
    private GameObject scoreChangePanel;
    [SerializeField]
    private Text playerOldScoreText;
    [SerializeField]
    private Text playerNewScoreText;
    [SerializeField]
    private Text aiOldScoreText;
    [SerializeField]
    private Text aiNewScoreText;
    [SerializeField]
    private Button nextRoundButton;
    [Header("Other")]
    [SerializeField]
    private ModePanelController modeController;
    [Space]
    [SerializeField]
    private float phaseDelay = 2f;
    [SerializeField]
    private Sprite[] weaponSprites;

    private int[] scores = new int[2];
    private Weapon playerWeapon;
    private Weapon aiWeapon;

    private enum Weapon
    {
        Stone,
        Paper,
        Scissors
    }

    private void Start()
    {
        ResetUi();

        //weaponButtons
    }

    public void ResetUi()
    {
        weaponPanel.SetActive(true);
        aiWeaponPanel.SetActive(false);
        resultPanel.SetActive(false);
        scoreChangePanel.SetActive(false);
        score1Text.text = scores[0].ToString();
        score2Text.text = scores[1].ToString();
    }

    public void ButtonWeapon(int weapon)
    {
        playerWeapon = (Weapon)weapon;
        StartCoroutine(RoundCoroutine());
    }

    private IEnumerator RoundCoroutine()
    {
        // ai choice phase
        weaponPanel.SetActive(false);
        aiWeaponPanel.SetActive(true);

        aiWeapon = ChooseAiWeapon();
        playerWeaponImage.sprite = weaponSprites[(int)playerWeapon];
        aiWeaponImage.sprite = weaponSprites[(int)aiWeapon];

        yield return new WaitForSeconds(phaseDelay);

        // result phase
        aiWeaponPanel.SetActive(false);
        resultPanel.SetActive(true);
        playerWeaponResultImage.sprite = weaponSprites[(int)playerWeapon];
        aiWeaponResultImage.sprite = weaponSprites[(int)aiWeapon];
        bool isWin = IsWin(playerWeapon, aiWeapon);
        resultText.text = isWin ? "WIN" : "LOSE";
        signTransform.localEulerAngles = new Vector3(signTransform.localEulerAngles.x, signTransform.localEulerAngles.y, isWin ? 90f : -90f);

        yield return new WaitForSeconds(phaseDelay);

        // score change phase        
        resultPanel.SetActive(false);
        scoreChangePanel.SetActive(true);
        nextRoundButton.interactable = false;

        playerOldScoreText.text = scores[0].ToString();
        aiOldScoreText.text = scores[1].ToString();
        if (isWin)
        {
            scores[0]++;
        }
        else
        {
            scores[1]++;
        }
        playerNewScoreText.text = scores[0].ToString();
        aiNewScoreText.text = scores[1].ToString();

        Transform trOld = isWin ? playerOldScoreText.transform : aiOldScoreText.transform;
        Transform trNew = isWin ? playerNewScoreText.transform : aiNewScoreText.transform;

        for (float scale = 0f; scale <= 1f; scale += 1f * Time.deltaTime)
        {
            trNew.localScale = new Vector3(trNew.localScale.x, scale, trNew.localScale.z);
            trOld.localScale = new Vector3(trOld.localScale.x, 1f - scale, trOld.localScale.z);
            yield return new WaitForEndOfFrame();
        }

        nextRoundButton.interactable = true;
        score1Text.text = scores[0].ToString();
        score2Text.text = scores[1].ToString();
    }

    private Weapon ChooseAiWeapon()
    {
        Weapon result = Weapon.Paper;

        if (modeController.IsFairMode)
        {
            do
            {
                result = (Weapon)Random.Range(0, 3);
            }
            while (result == playerWeapon);

            return result;
        }

        bool win = Random.Range(0f, 1f) < modeController.Chance;

        switch (playerWeapon)
        {
            case Weapon.Paper:
                result = win ? Weapon.Scissors : Weapon.Stone;
                break;
            case Weapon.Scissors:
                result = win ? Weapon.Stone : Weapon.Paper;
                break;
            case Weapon.Stone:
                result = win ? Weapon.Paper : Weapon.Scissors;
                break;
        }                
        return result;
    }

    private bool IsWin(Weapon player, Weapon ai)
    {
        int mod = ((int)player - (int)ai) % 3;
        return mod == 1 || mod == -2;
    }
}
