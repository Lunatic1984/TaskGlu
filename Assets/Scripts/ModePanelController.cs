﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModePanelController : MonoBehaviour
{
    [SerializeField]
    private Text modeText;
    [SerializeField]
    private Button fairButton;
    [SerializeField]
    private Button notFairButton;
    [SerializeField]
    private InputField chanceInputField;

    private bool isFairMode;
    public bool IsFairMode
    {
        get
        {
            return isFairMode;
        }
        private set
        {
            isFairMode = value;
            notFairButton.interactable = value;
            fairButton.interactable = !value;
            chanceInputField.interactable = !value;
            modeText.text = value ? "FAIR MODE" : "NOT FAIR MODE";
        }
    }

    public float Chance { get; private set; }

    private void Start()
    {
        IsFairMode = true;

        notFairButton.onClick.AddListener(() => IsFairMode = false);

        fairButton.onClick.AddListener(() => IsFairMode = true);

        chanceInputField.onEndEdit.AddListener((s) =>
        {
            float chance = 0.5f;
            if (float.TryParse(s, out chance))
            {
                Chance = Mathf.Clamp01(chance);
            }
            else
            {
                Chance = 0.5f;
            }

            chanceInputField.text = Chance.ToString();
        });
    }
}
